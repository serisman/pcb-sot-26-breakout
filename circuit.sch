EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:IC
LIBS:circuit-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "20 jul 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_3 K1
U 1 1 53CB049B
P 5150 3950
F 0 "K1" V 5100 3950 50  0000 C CNN
F 1 "CONN_3" V 5200 3950 40  0000 C CNN
F 2 "" H 5150 3950 60  0000 C CNN
F 3 "" H 5150 3950 60  0000 C CNN
	1    5150 3950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_3 K2
U 1 1 53CB04AA
P 6750 3950
F 0 "K2" V 6700 3950 50  0000 C CNN
F 1 "CONN_3" V 6800 3950 40  0000 C CNN
F 2 "" H 6750 3950 60  0000 C CNN
F 3 "" H 6750 3950 60  0000 C CNN
	1    6750 3950
	1    0    0    1   
$EndComp
$Comp
L IC-6 U1
U 1 1 53CB06FA
P 5950 3950
F 0 "U1" H 5950 4250 60  0000 C CNN
F 1 "IC-6" H 5950 3650 60  0000 C CNN
F 2 "~" H 5950 3950 60  0000 C CNN
F 3 "~" H 5950 3950 60  0000 C CNN
	1    5950 3950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
