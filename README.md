# SOT-26 Breakout v1.0 #

## Schematic ##

![Schematic.png](https://bytebucket.org/serisman/pcb-sot-26-breakout/raw/master/output/Schematic.png)

## Design Rules ##

* Clearance: 7 mil
* Track Width: 20 mil
* Via Diameter: 27 mil
* Via Drill: 13 mil
* Zone Clearance: 7 mil
* Zone Min Width: 7 mil
* Zone Thermal Antipad Clearance: 7 mil
* Zone Thermal Spoke Width: 15 mil

## PCB available on OSH Park ##

* 0.4" x 0.3" (10.21 mm x 7.65 mm)
* $0.20 each ($0.60 for 3)
* [https://oshpark.com/shared_projects/7DPhEWTQ](https://oshpark.com/shared_projects/7DPhEWTQ)

### PCB Front ###

![PCB - Front.png](https://bytebucket.org/serisman/pcb-sot-26-breakout/raw/master/output/PCB%20-%20Front.png)

### PCB Back ###

![PCB - Back.png](https://bytebucket.org/serisman/pcb-sot-26-breakout/raw/master/output/PCB%20-%20Back.png)